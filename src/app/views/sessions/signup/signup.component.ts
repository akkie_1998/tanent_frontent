import { Component, OnInit, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatProgressBar } from '@angular/material/progress-bar';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import {JwtAuthService}  from '../../../shared/services/auth/jwt-auth.service'
import { CommonService } from '../../../shared/services/common-service/common.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signupForm: FormGroup
  constructor(
    private jwtAuthService: JwtAuthService,
    private commonService: CommonService
    ) {}

  ngOnInit() {
    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signupForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: password,
      confirmPassword: confirmPassword,
      agreed: new FormControl('', (control: FormControl) => {
        const agreed = control.value;
        if(!agreed) {
          return { agreed: true }
        }
        return null;
      })
    })
    this.getCountries()

  }

  getCountries(){
    this.commonService.getCountries().subscribe((res)=>{
      console.log("country-------res",res)
    })

  }

  signup() {
    const signupData = this.signupForm.value;
    console.log(signupData);

    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';

    const data = {
      email: this.signupForm.value.email,
      password : this.signupForm.value.password,
      confirmPassword : this.signupForm.value.confirmPassword,
    }

    console.log("send data------",data)

    if(this.signupForm.valid){


    try {
      this.jwtAuthService.signUp(data).subscribe(res=>{
        console.log("response=----------",res)
      })
    } catch (e) {
      console.log("error------",e)
    }
    
    }
  }

}
