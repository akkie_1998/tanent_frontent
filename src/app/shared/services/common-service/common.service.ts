import { Injectable } from '@angular/core';
import { environment } from "environments/environment";
import { HttpClient } from '@angular/common/http';
import { of, BehaviorSubject, throwError } from "rxjs";
import { map, catchError, delay } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  apiUrl: string = environment.apiURL;

  constructor(
    private http: HttpClient
          ) { }
   getCountries() {
    // return of({token: DEMO_TOKEN, user: DEMO_USER})
    return this.http.get(`${this.apiUrl}/Countries/GetCountries`)
      .pipe(
        delay(1000),
        map((res: any) => {
          return res;
        }),
        catchError((error) => {
          return throwError(error);
        })
      );
  }

}
